# Everest Agent

The agent implements integration of external computing resources with Everest platform. It runs on a resource and acts as a mediator between Everest and the resource. 

This approach has a number of advantages in comparison to plain SSH access such as supporting resources without inbound connectivity (behind a firewall or NAT) and ability to control actions performed by Everest on the resource. However, such approach requires the manual deployment of the agent on each resource. To mitigate this disadvantage the developed agent has minimal software requirements and is easy to deploy by an unprivileged user.

The agent supports integration with various types of resources via adapter mechanism. At the moment the following adapters are implemented:

* local - running tasks on a local server,
* docker - running tasks on a local server inside Docker containers,
* torque - running tasks on a TORQUE cluster,
* slurm - running tasks on a SLURM cluster.

In the two latter cases an agent is running on a submission host of the cluster.

The communication between an agent and the platform is implemented through the WebSocket protocol. Upon startup an agent initiates connection with the platform to establish a bidirectional communication channel. This channel is used only for task control and status messages exchanged between Everest and an agent. Task data transfer is performed by an agent via the HTTP protocol. Authentication of an agent is performed by passing a secret token issued by Everest.

In order to attach a resource a user should register the resource in Everest and run the agent on the resource with configuration including the obtained token. After the agent is connected to Everest it starts to send information about the resource state that is displayed in the Web UI.

## Documentation

* [User Manual](docs/manual.adoc)