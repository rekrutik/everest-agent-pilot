#!/bin/bash
BIN_DIR=`dirname "$(cd ${0%/*} && echo $PWD/${0##*/})"`
cd $BIN_DIR

./stop.sh
./start.sh $@