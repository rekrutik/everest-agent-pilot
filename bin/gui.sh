#!/bin/bash
BIN_DIR=`dirname "$(cd ${0%/*} && echo $PWD/${0##*/})"`
AGENT_HOME=`dirname "$BIN_DIR"`
cd $AGENT_HOME

# Find latest Python 2.x version
PYTHON=$(ls /usr/bin/python2.? | tail -n1)
# Make sure Python is 2.6 or later
PYTHON_OK=`$PYTHON -c 'import sys; print (sys.version_info >= (2, 6) and "1" or "0")'`
if [ "$PYTHON_OK" = '0' ]; then
    echo "Python versions < 2.6 are not supported"
    exit 1
fi
PYTHONPATH=$(dirname $0)/..:$PYTHONPATH $PYTHON -m everest_agent.gui.main $@