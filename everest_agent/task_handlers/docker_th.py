#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2013 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from __future__ import absolute_import
import hashlib
import json
import os
import subprocess

from everest_agent.exceptions import TaskException
import six


def create(config):
    return DockerTaskHandler(config)


class DockerTaskHandler:
    def __init__(self, config):
        try:
            p = subprocess.Popen(["id", "-u"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            (out, err) = p.communicate()
        except Exception as e:
            raise e
        else:
            self.user = out.strip()
        self.image = config['image']
        self.mem_limit = config['mem_limit']
        if 'network' in config:
            self.network = config['network']
        else:
            self.network = None
        
    def get_type(self):
        return 'docker'

    # TODO: support resource requirements
    def submit(self, command, work_dir, env, requirements):
        task_image = self.image
        if not os.access(work_dir, os.F_OK):
            os.makedirs(work_dir)
        else:
            dockerfile = work_dir + '/Dockerfile'
            if os.path.isfile(dockerfile):
                # generate image id
                with open(dockerfile) as f:
                    data = f.read()
                    md5 = hashlib.md5(data).hexdigest()
                    task_image = 'everest/custom-' + md5

                # check if image exists
                try:
                    inspect_result = subprocess.call(['docker', 'inspect', task_image],
                                                     stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                except Exception:
                    raise TaskException("Failed to inspect Docker image: %s" % task_image)

                if inspect_result != 0:
                    # build image
                    # TODO: build image asynchronously to avoid blocking the agent
                    try:
                        p = subprocess.Popen(['docker', 'build', '-t', task_image, work_dir],
                                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                        p.communicate()
                        if p.returncode != 0:
                            raise TaskException("Unable to build Docker image")
                    except Exception:
                        raise TaskException("Unable to build Docker image")

        run_cmd = ["docker", "run"]
        env['HOME'] = '/task'
        for key, val in six.iteritems(env):
            run_cmd.append('-e')
            run_cmd.append('%s=%s' % (key, val))
        if self.network is not None:
            run_cmd.append('--network')
            run_cmd.append(self.network)
        run_cmd += ["-d",
                    "-m=" + self.mem_limit,
                    "-u", self.user,
                    "-v", work_dir + ":/task:rw",
                    "-w", "/task",
                    task_image,
                    "/bin/sh", "-c", command.encode('ascii') + " >stdout 2>stderr"]
        try:
            p = subprocess.Popen(run_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            (out, err) = p.communicate()
            container_id = out.strip()
            return container_id
        except Exception as e:
            raise TaskException("Unable to start container (%s)" % e)

    def get_state(self, container_id):
        try:
            p = subprocess.Popen(["docker", "inspect", container_id],
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            (resp, er) = p.communicate()
            r = json.loads(resp)
        except Exception:
            raise TaskException("Unable to read container state")
        if r[0]['State']['Running']:
            return "RUNNING"
        elif r[0]['State']['Status'] == 'exited':
            # remove completed container
            subprocess.call(['docker', 'rm', container_id],
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            if r[0]['State']['ExitCode'] == 0:
                return "COMPLETED"
            elif r[0]['State']['ExitCode'] < 0:
                return "CANCELED"
            else:
                st = "FAILED ExitCode=" + str(r[0]['State']['ExitCode'])
                return st

    def cancel(self, container_id):
        try:
            p = subprocess.Popen(["docker", "kill", str(container_id)])
            p.wait()
        except:
            pass
